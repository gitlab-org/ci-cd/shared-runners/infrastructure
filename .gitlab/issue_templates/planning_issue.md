This issue is used by ~"group::hosted runners" to plan upcoming iterations. [All planning issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=title_asc&state=opened&label_name%5B%5D=Category%3ARunner%20SaaS&label_name%5B%5D=Planning%20Issue&first_page_size=20).

KRs:


## OnCall :pager:

## Deliverables :fox:

### :star2: Features

~"Runner::P1"

~"Runner::P2"

~"Runner::P3"

### :construction: Maintenance


### :rotating_light: Bugs


### :bulb: R&D


/label ~"group::hosted runners" ~"Category:Hosted Runners" ~"devops::verify" ~"section::ci" ~"Planning Issue"
/assign @gabrielengel_gl
